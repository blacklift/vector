package com.blacklift.vector.app.ui.base


/**
 * Created by rogergarzon on 29/3/18.
 */
interface IOnItemClickListener<in T> {
    fun onItemClick(t: T?)
}