package com.blacklift.vector.app.ui.users

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.blacklift.vector.R
import com.blacklift.vector.app.ui.MainActivity
import com.blacklift.vector.app.ui.base.IOnItemClickListener
import com.blacklift.vector.app.ui.user.UserFragment
import com.blacklift.vector.di.helpers.Injectable
import com.blacklift.vector.domain.model.Users
import com.blacklift.vector.domain.model.base.Status
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_users.*
import javax.inject.Inject

class UsersFragment : Fragment(), Injectable, IOnItemClickListener<Users> {


    @Inject
    lateinit var viewModel: UsersViewModel


    companion object {
        const val TAG = "UsersFragment"
        fun newInstance() : UsersFragment{
            return UsersFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       return inflater.inflate(R.layout.fragment_users, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureRecyclerView()
        observeUsers()
    }

    private fun observeUsers(){
        viewModel.getUsers().observe(viewLifecycleOwner,
                Observer { resource ->
                    resource?.let {
                        when {
                            it.status == Status.LOADING -> {
                                showProgress()
                            }
                            it.status == Status.SUCCESS -> {
                                hideProgress()
                                refreshRecyclerView(it.data ?: listOf())
                            }
                            else -> {
                                hideProgress()
                                showMessage(it.message?: getString(R.string.error_message_unknown))
                            }
                        }
                    }

                })
    }



    private fun configureRecyclerView(){
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(
            DividerItemDecoration(context!!,
                DividerItemDecoration.VERTICAL)
        )
        val adapter = UsersAdapter(listOf(), this)
        recyclerView.adapter = adapter
    }

    private fun refreshRecyclerView(items : List<Users>){
        val adapter = UsersAdapter( items , this)
        recyclerView.swapAdapter(adapter, false)
    }


    override fun onItemClick(t: Users?) {
        val f = UserFragment.newInstance(t?.name!!)
        fragmentManager
            ?.beginTransaction()
            ?.replace(R.id.container, f, f.tag)
            ?.addToBackStack(null)
            ?.commit()
    }

    private fun showProgress(){
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgress(){
        progressBar.visibility = View.GONE
    }

    private fun showMessage(message : String){
        Snackbar.make(recyclerView, message, Snackbar.LENGTH_LONG ).show()
    }


}