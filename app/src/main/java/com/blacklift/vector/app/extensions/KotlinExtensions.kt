package com.blacklift.vector.app.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import com.blacklift.vector.domain.model.base.Resource
import io.reactivex.Flowable

/**
 * Created by rogergarzon on 29/3/18.
 */
object KotlinExtensions {

    fun <T> Flowable<T>.asLiveData(t : T?): LiveData<Resource<T>> {
        val flowable = this.map{Resource.success(it)}
                .startWith(Resource.loading(t))
                .onErrorReturn{Resource.error(it.message?: "", null)}
        return LiveDataReactiveStreams.fromPublisher(flowable)
    }



}