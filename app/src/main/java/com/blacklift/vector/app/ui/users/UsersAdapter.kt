package com.blacklift.vector.app.ui.users

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.blacklift.vector.R
import com.blacklift.vector.app.ui.base.IOnItemClickListener
import com.blacklift.vector.domain.model.Users
import kotlinx.android.synthetic.main.user_item.view.*


class UsersAdapter(private val items: List<Users>, private val listener: IOnItemClickListener<Users>) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false)
            return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }



    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Users){
            itemView.tv_name.text = item.name
            itemView.tv_surname.text = item.url
            bindImage(item.image)
            itemView.setOnClickListener{listener.onItemClick(item)}

        }

        private fun bindImage(image: String?){
            val requestOptions = RequestOptions
                    .circleCropTransform()
                    .placeholder(R.drawable.photo_placeholder)
                    .error(R.drawable.photo_placeholder)

            Glide.with(itemView.context).load(image)
                    .apply(requestOptions)
                    .transition(GenericTransitionOptions.with(R.anim.zoom_in))
                    .into(itemView.imageView)
        }

    }

}