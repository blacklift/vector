package com.blacklift.vector.app.base

import timber.log.Timber



/**
 * Created by Avantgarde on 26/04/2018.
 */
class NotLoggingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, throwable: Throwable?) {}
}