package com.blacklift.vector.app.ui.mapper.base

interface DomainToUiMapper <Domain, Ui>{
    fun mapDomainToUi(domain: Domain) : Ui
}