package com.blacklift.vector.app.ui.users

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.blacklift.vector.domain.interactor.GetUsersUseCase
import com.blacklift.vector.domain.model.Users
import com.blacklift.vector.domain.model.base.Resource
import com.blacklift.vector.app.extensions.KotlinExtensions.asLiveData
import javax.inject.Inject

class UsersViewModel @Inject constructor(private val getUsersUseCase: GetUsersUseCase
) : ViewModel() {


    fun getUsers(): LiveData<Resource<List<Users>>> {
       val flowable = getUsersUseCase.execute()
        return flowable.asLiveData(null)
    }
}