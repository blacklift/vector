package com.blacklift.vector.app.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.blacklift.vector.R
import com.blacklift.vector.di.helpers.Injectable
import com.blacklift.vector.domain.model.User
import com.blacklift.vector.domain.model.base.Status
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_user.*
import kotlinx.android.synthetic.main.fragment_user.progressBar
import kotlinx.android.synthetic.main.fragment_users.*
import kotlinx.android.synthetic.main.user_item.view.*
import javax.inject.Inject


class UserFragment: Fragment(), Injectable  {

    @Inject
    lateinit var viewModel: UserViewModel

    var user: String? = null

    companion object {
        const val TAG = "UserFragment"
        fun newInstance(user: String) : UserFragment{
            val f = UserFragment()
            val args = Bundle()
            args.putString("USER", user)
            f.arguments = args
            return f
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        user = arguments?.getString("USER")
        observeUser()
    }

    fun showUser(user: User) {
        tv_name.text = user.name
        tv_url.text = user.url
        tv_company.text = user.company
        tv_location.text = user.location
        tv_email.text = user.email
        tv_name.text = user.name

        Glide.with(imageView.context).load(user.image)
            .transition(GenericTransitionOptions.with(R.anim.zoom_in))
            .into(imageView)

    }

    private fun observeUser(){
        viewModel.getUser(user!!).observe(viewLifecycleOwner,
            Observer { resource ->
                resource?.let {
                    when {
                        it.status == Status.LOADING -> {
                            showProgress()
                        }
                        it.status == Status.SUCCESS -> {
                            hideProgress()
                            showUser(resource.data!!)
                        }
                        else -> {
                            hideProgress()
                            showMessage(it.message?: getString(R.string.error_message_unknown))
                        }
                    }
                }

            })
    }


    private fun showProgress(){
        progressBar.visibility = View.VISIBLE
        content.visibility = View.GONE
    }

    private fun hideProgress(){
        progressBar.visibility = View.GONE
        content.visibility = View.VISIBLE
    }

    private fun showMessage(message : String){
        Snackbar.make(content, message, Snackbar.LENGTH_LONG ).show()
    }
}