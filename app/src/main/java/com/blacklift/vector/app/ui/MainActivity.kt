package com.blacklift.vector.app.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.blacklift.vector.R
import com.blacklift.vector.app.ui.users.UsersFragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(R.id.container, UsersFragment.newInstance(), UsersFragment.TAG, false)
    }

    public fun replaceFragment(id: Int, f: Fragment, tag: String?, addToBackStack: Boolean) {
        val ft = supportFragmentManager
                .beginTransaction()
                .replace(id, f, tag)
        if (addToBackStack)
            ft.addToBackStack(tag)
        ft.commit()
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector
}
