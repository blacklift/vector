package com.blacklift.vector.app.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.blacklift.vector.domain.model.base.Resource
import com.blacklift.vector.app.extensions.KotlinExtensions.asLiveData
import com.blacklift.vector.domain.interactor.GetUserUseCase
import com.blacklift.vector.domain.model.User
import javax.inject.Inject

class UserViewModel @Inject constructor(private val getUserUseCase: GetUserUseCase
) : ViewModel() {


    fun getUser(login: String): LiveData<Resource<User>> {
       val flowable = getUserUseCase.execute(login)
        return flowable.asLiveData(null)
    }
}