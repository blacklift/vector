/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blacklift.vector.data.base

import io.reactivex.Flowable

import io.reactivex.schedulers.Schedulers
import retrofit2.Response

/**
 * Class that implements Single Source of Truth pattern.
 */

abstract class NetworkBoundResource<Api, Model, Domain> {

    private val flowable : Flowable<Domain>

    init{
        flowable = fetch()
    }

    fun asFlowable() = flowable

    private fun fetch(): Flowable<Domain> {
        return fetchFromNetwork()
    }

    private fun fetchFromNetwork(): Flowable<Domain> {
        val apiResponse = createCall()
        return apiResponse
                .subscribeOn(Schedulers.io())
                .doOnNext{
                    if (it.isSuccessful) {
                        val model = mapApiToModel(it.body())
                        saveCallResult(model)
                    }
                }
                .flatMap {
                    fetchFromLocal()
                }
    }

    private fun fetchFromLocal(): Flowable<Domain> {
        return loadFromDb().map { mapModelToDomain(it) }
    }

    protected abstract fun createCall(): Flowable<Response<Api>>

    protected abstract fun loadFromDb(): Flowable<Model>

    protected abstract fun saveCallResult(model: Model)

    protected abstract fun shouldFetch(model: Model?): Boolean

    protected abstract fun mapApiToModel(api: Api?): Model

    protected abstract fun mapModelToDomain(model: Model): Domain

}
