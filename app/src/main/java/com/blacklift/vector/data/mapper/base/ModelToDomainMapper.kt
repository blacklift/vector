package com.blacklift.vector.data.mapper.base

/**
 * Created by Avantgarde on 14/03/2018.
 */
interface ModelToDomainMapper<Model, Domain> {
    fun mapModelToDomain(model: Model): Domain
}