package com.blacklift.vector.data.mapper.base

/**
 * Created by Avantgarde on 14/03/2018.
 */
interface ApiToModelMapper<Api, Model> {
    fun mapApiToModel(api: Api): Model
}