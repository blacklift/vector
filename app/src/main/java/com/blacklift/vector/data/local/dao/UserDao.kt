package com.blacklift.vector.data.local.dao

import androidx.room.*
import com.blacklift.vector.data.local.model.UserModel
import io.reactivex.Single

@Dao
interface UserDao {
    @Query("SELECT * FROM User WHERE login = :login LIMIT 1")
    fun findUser(login: String): Single<UserModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: UserModel)

    @Update
    fun update(user: UserModel)

    @Delete
    fun delete(user: UserModel)

}