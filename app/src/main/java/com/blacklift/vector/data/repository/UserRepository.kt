package com.blacklift.vector.data.repository

import com.blacklift.vector.data.base.NetworkBoundResource
import com.blacklift.vector.data.local.dao.UserDao
import com.blacklift.vector.data.local.model.UserModel
import com.blacklift.vector.data.mapper.data.UserMapper
import com.blacklift.vector.data.mapper.domain.UserDomainMapper
import com.blacklift.vector.data.server.Api
import com.blacklift.vector.data.server.model.UserServerModel
import com.blacklift.vector.domain.model.User
import com.blacklift.vector.domain.repository.IUserRepository
import io.reactivex.Flowable
import retrofit2.Response
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val apiToModelMapper: UserMapper,
    private val modelToDomainMapper: UserDomainMapper,
    private val api: Api,
    private val dao: UserDao
) : IUserRepository {

    override fun getUser(login: String): Flowable<User> {
        return object : NetworkBoundResource<UserServerModel, UserModel, User>() {
            override fun createCall(): Flowable<Response<UserServerModel>> {
                return api.getUser(login).toFlowable()
            }

            override fun loadFromDb(): Flowable<UserModel> {
                return dao.findUser(login).toFlowable()
            }

            override fun saveCallResult(model: UserModel) {
                dao.insert(model)
            }

            override fun shouldFetch(model: UserModel?): Boolean {
                return true
            }

            override fun mapApiToModel(api: UserServerModel?): UserModel {
                return apiToModelMapper.mapApiToModel(api!!)
            }

            override fun mapModelToDomain(model: UserModel): User {
                return modelToDomainMapper.mapModelToDomain(model)
            }

        }.asFlowable()
    }
}