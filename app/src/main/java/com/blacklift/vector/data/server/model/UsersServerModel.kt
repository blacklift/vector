package com.blacklift.vector.data.server.model

import com.google.gson.annotations.SerializedName


data class UsersServerModel(
    @SerializedName("login") var name: String,
    @SerializedName("avatar_url") var image: String? = null,
    @SerializedName("url") var url: String? = null
)


