package com.blacklift.vector.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "User")
data class UserModel (
    @PrimaryKey
    var login: String,
    var image: String? = null,
    var url: String? = null,
    var name: String? = null,
    var company: String? = null,
    var location: String? = null,
    var email: String? = null
)