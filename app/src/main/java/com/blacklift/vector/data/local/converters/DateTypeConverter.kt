package com.blacklift.vector.data.local.converters

import androidx.room.TypeConverter
import java.util.*


/**
 * Created by Avantgarde on 22/03/2018.
 */
class DateTypeConverter {
    @TypeConverter
    fun toDate(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun toLong(value: Date?): Long? {
        return if (value == null) null else value.time
    }
}