package com.blacklift.vector.data.repository

import com.blacklift.vector.data.base.NetworkBoundResource
import com.blacklift.vector.data.local.dao.UsersDao
import com.blacklift.vector.data.local.model.UsersModel
import com.blacklift.vector.data.mapper.data.UsersMapper
import com.blacklift.vector.data.mapper.domain.UsersDomainMapper
import com.blacklift.vector.data.server.Api
import com.blacklift.vector.data.server.model.UsersServerModel
import com.blacklift.vector.domain.model.Users
import com.blacklift.vector.domain.repository.IUsersRepository
import io.reactivex.Flowable
import retrofit2.Response
import javax.inject.Inject

class UsersRepository @Inject constructor(
    private val apiToModelMapper: UsersMapper,
    private val modelToDomainMapper: UsersDomainMapper,
    private val api: Api,
    private val dao: UsersDao
) : IUsersRepository {

    override fun getUsers(): Flowable<List<Users>> {
        return object : NetworkBoundResource<List<UsersServerModel>, List<UsersModel>, List<Users>>() {
            override fun createCall(): Flowable<Response<List<UsersServerModel>>> {
                return api.getUsers().toFlowable()
            }

            override fun loadFromDb(): Flowable<List<UsersModel>> {
                return dao.getAll()
            }

            override fun saveCallResult(model: List<UsersModel>) {
                dao.insert(model)
            }

            override fun shouldFetch(model: List<UsersModel>?): Boolean {
                //return model?.isEmpty() ?: true
                return true
            }

            override fun mapApiToModel(api: List<UsersServerModel>?): List<UsersModel> {
                return api?.map {apiToModelMapper.mapApiToModel(it)} ?: listOf()
            }

            override fun mapModelToDomain(model: List<UsersModel>): List<Users> {
                return model.map {modelToDomainMapper.mapModelToDomain(it)}
            }

        }.asFlowable()
    }
}