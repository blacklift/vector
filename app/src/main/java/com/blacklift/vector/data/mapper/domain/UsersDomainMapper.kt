package com.blacklift.vector.data.mapper.domain

import com.blacklift.vector.data.local.model.UsersModel
import com.blacklift.vector.data.mapper.base.ModelToDomainMapper
import com.blacklift.vector.domain.model.Users
import javax.inject.Inject

class UsersDomainMapper @Inject constructor() : ModelToDomainMapper<UsersModel, Users>{
    override fun mapModelToDomain(model: UsersModel): Users {
        return Users(
            name=model.name,
            url = model.url,
            image = model.image)
    }

}