package com.blacklift.vector.data.mapper.data

import com.blacklift.vector.data.local.model.UserModel
import com.blacklift.vector.data.mapper.base.ApiToModelMapper
import com.blacklift.vector.data.server.model.UserServerModel
import javax.inject.Inject

class UserMapper @Inject constructor() : ApiToModelMapper<UserServerModel, UserModel>{

    override fun mapApiToModel(api: UserServerModel): UserModel {
        return UserModel(
                login =api.login,
                image = api.image,
                url =  api.url,
                name = api.name,
                company = api.company,
                location = api.location,
                email = api.email
        )
    }

}