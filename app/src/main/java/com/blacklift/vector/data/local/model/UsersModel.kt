package com.blacklift.vector.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Users")
data class UsersModel(
        @PrimaryKey
        var name: String,
        var image: String? = null,
        var url: String? = null
)

