package com.blacklift.vector.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.blacklift.vector.data.local.converters.DateTypeConverter
import com.blacklift.vector.data.local.dao.UserDao
import com.blacklift.vector.data.local.dao.UsersDao
import com.blacklift.vector.data.local.model.UserModel
import com.blacklift.vector.data.local.model.UsersModel

/**
 * Created by Avantgarde on 22/03/2018.
 */

@Database(entities = [
    UsersModel::class,
    UserModel::class
], version = 1)
@TypeConverters(DateTypeConverter::class)
abstract class AppDB : RoomDatabase() {
    abstract fun usersDao() : UsersDao
    abstract fun userDao() : UserDao

}