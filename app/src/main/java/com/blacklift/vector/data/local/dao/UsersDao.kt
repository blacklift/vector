package com.blacklift.vector.data.local.dao

import androidx.room.*
import com.blacklift.vector.data.local.model.UsersModel
import io.reactivex.Flowable

@Dao
interface UsersDao {
    @Query("SELECT * FROM Users ORDER BY name ASC")
    fun getAll(): Flowable<List<UsersModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(sports: List<UsersModel>)

    @Update
    fun update(sports: List<UsersModel>)

    @Delete
    fun delete(sports: List<UsersModel>)

}