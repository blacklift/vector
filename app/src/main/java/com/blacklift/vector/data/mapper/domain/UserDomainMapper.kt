package com.blacklift.vector.data.mapper.domain

import com.blacklift.vector.data.local.model.UserModel
import com.blacklift.vector.data.mapper.base.ApiToModelMapper
import com.blacklift.vector.data.mapper.base.ModelToDomainMapper
import com.blacklift.vector.data.server.model.UserServerModel
import com.blacklift.vector.domain.model.User
import javax.inject.Inject

class UserDomainMapper @Inject constructor() : ModelToDomainMapper<UserModel, User>{
    override fun mapModelToDomain(model: UserModel): User {
        return User(
            login =model.login,
            image = model.image,
            url =  model.url,
            name = model.name,
            company = model.company,
            location = model.location,
            email = model.email
        )
    }

}