package com.blacklift.vector.data.server

import com.blacklift.vector.data.server.model.UsersServerModel
import com.blacklift.vector.data.server.model.UserServerModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path


/**
 * Created by Avantgarde on 21/03/2018.
 */
interface Api {
    @GET("/users")
    fun getUsers(): Single<Response<List<UsersServerModel>>>

    @GET("/users/{user}")
    fun getUser(@Path("user") user: String): Single<Response<UserServerModel>>
}