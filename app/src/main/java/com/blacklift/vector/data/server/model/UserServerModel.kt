package com.blacklift.vector.data.server.model

import com.google.gson.annotations.SerializedName

data class UserServerModel (
    @SerializedName("login") var login: String,
    @SerializedName("avatar_url") var image: String? = null,
    @SerializedName("url") var url: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("company") var company: String? = null,
    @SerializedName("location") var location: String? = null,
    @SerializedName("email") var email: String? = null
)