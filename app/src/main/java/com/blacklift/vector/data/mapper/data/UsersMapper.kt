package com.blacklift.vector.data.mapper.data

import com.blacklift.vector.data.local.model.UsersModel
import com.blacklift.vector.data.mapper.base.ApiToModelMapper
import com.blacklift.vector.data.server.model.UsersServerModel
import javax.inject.Inject

class UsersMapper @Inject constructor() : ApiToModelMapper<UsersServerModel, UsersModel>{

    override fun mapApiToModel(api: UsersServerModel): UsersModel {
        return UsersModel(
                image = api.image,
                name =  api.name,
                url = api.url)
    }

}