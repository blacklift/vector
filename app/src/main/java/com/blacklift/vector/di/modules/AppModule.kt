/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blacklift.vector.di.modules


import android.app.Application
import android.content.Context
import androidx.room.Room
import com.blacklift.vector.data.local.AppDB
import com.blacklift.vector.data.local.dao.UserDao
import com.blacklift.vector.data.local.dao.UsersDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
internal class AppModule {

    @Singleton
    @Provides
    fun provideDb(app: Application): AppDB {
        return Room.databaseBuilder(app, AppDB::class.java, "app.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    @Provides
    @Singleton
    fun provideApplicationContext(app: Application): Context {
        return app
    }

    @Singleton
    @Provides
    fun provideUsersDao(db: AppDB): UsersDao {
        return db.usersDao()
    }


    @Singleton
    @Provides
    fun provideUserDao(db: AppDB): UserDao {
        return db.userDao()
    }
}
