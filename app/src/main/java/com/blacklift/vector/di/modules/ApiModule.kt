package com.blacklift.vector.di.modules

import com.blacklift.vector.BuildConfig
import com.blacklift.vector.data.server.Api
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Avantgarde on 23/03/2018.
 */
@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideApi(): Api {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(Api::class.java)
    }
}