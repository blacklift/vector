package com.blacklift.vector.di.modules

import com.blacklift.vector.data.repository.UserRepository
import com.blacklift.vector.data.repository.UsersRepository
import com.blacklift.vector.domain.repository.IUserRepository
import com.blacklift.vector.domain.repository.IUsersRepository
import dagger.Binds
import dagger.Module

/**
 * Created by Avantgarde on 23/03/2018.
 */
@Module
abstract class RepositoryModule {
    @Binds
    abstract fun providesUsersRepository(repository: UsersRepository): IUsersRepository

    @Binds
    abstract fun providesUserRepository(repository: UserRepository): IUserRepository
}