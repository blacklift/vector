package com.blacklift.vector.di.scopes

import javax.inject.Scope


/**
 * Created by Avantgarde on 13/04/2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope