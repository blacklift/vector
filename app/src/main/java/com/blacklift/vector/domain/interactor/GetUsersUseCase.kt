package com.blacklift.vector.domain.interactor

import com.blacklift.vector.domain.interactor.base.FlowableUseCase
import com.blacklift.vector.domain.model.Users
import com.blacklift.vector.domain.repository.IUsersRepository
import io.reactivex.Flowable
import javax.inject.Inject

class GetUsersUseCase @Inject constructor(private val repository: IUsersRepository)
    : FlowableUseCase<List<Users>>{

    override fun execute(): Flowable<List<Users>> {
        return repository.getUsers()
    }
}
