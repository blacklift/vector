package com.blacklift.vector.domain.interactor.base

import io.reactivex.Flowable

/**
 * Created by rogergarzon on 29/3/18.
 */
interface FlowableUseCaseWithParameter<P, R> {
    fun execute(p: P): Flowable<R>
}