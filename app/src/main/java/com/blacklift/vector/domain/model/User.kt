package com.blacklift.vector.domain.model


data class User(
        var login: String? = null,
        var image: String? = null,
        var url: String? = null,
        var name: String? = null,
        var company: String? = null,
        var location: String? = null,
        var email: String? = null
        )

