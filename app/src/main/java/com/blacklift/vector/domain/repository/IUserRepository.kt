package com.blacklift.vector.domain.repository

import com.blacklift.vector.domain.model.User
import io.reactivex.Flowable

interface IUserRepository {
    fun getUser(login: String): Flowable<User>
}