package com.blacklift.vector.domain.model


data class Users(
        var name: String? = null,
        var image: String? = null,
        var url: String? = null
)

