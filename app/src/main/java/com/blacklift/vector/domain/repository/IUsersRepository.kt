package com.blacklift.vector.domain.repository

import com.blacklift.vector.domain.model.Users
import io.reactivex.Flowable

interface IUsersRepository {
    fun getUsers(): Flowable<List<Users>>
}