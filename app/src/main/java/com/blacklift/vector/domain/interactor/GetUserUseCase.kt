package com.blacklift.vector.domain.interactor

import com.blacklift.vector.domain.interactor.base.FlowableUseCaseWithParameter
import com.blacklift.vector.domain.model.User
import com.blacklift.vector.domain.repository.IUserRepository
import io.reactivex.Flowable
import javax.inject.Inject

class GetUserUseCase @Inject constructor(private val repository: IUserRepository)
    : FlowableUseCaseWithParameter<String, User>{

    override fun execute(login: String): Flowable<User> {
        return repository.getUser(login)
    }
}
