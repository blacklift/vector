package com.blacklift.vector.domain.interactor.base

import io.reactivex.Flowable

/**
 * Created by rogergarzon on 29/3/18.
 */
interface FlowableUseCase<R> {
    fun execute(): Flowable<R>
}